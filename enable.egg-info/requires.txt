numpy
pillow
traits>=6.2.0
traitsui
pyface>=7.2.0
fonttools

[examples]
chaco
mayavi
scipy
kiwisolver
pyglet

[gl]
pygarrayimage
pyglet

[layout]
kiwisolver

[pdf]
reportlab

[svg]
pyparsing

[test]
hypothesis
PyPDF2
setuptools
